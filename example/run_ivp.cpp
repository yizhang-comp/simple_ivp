#include "simple_ivp.hpp"

#include <iostream>

int main() {
  double h = 0.15;
  double t = 0.00;
  int nsteps = 10;
  Eigen::VectorXd v0(1);
  v0 << 1.0;
  simple_ivp::IVPSolver<simple_ivp::Rk4, simple_ivp::RHSFunction> ivp(h, t, nsteps, v0);
  ivp.solve();
}


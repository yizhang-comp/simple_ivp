#ifndef IVP_SOLVER_HPP
#define IVP_SOLVER_HPP

#include <Eigen/Dense>
#include <iostream>

namespace simple_ivp{
  template<typename SteppingPolicy, typename RHSPolicy>
  class IVPSolver;
}

template<typename SteppingPolicy, typename RHSPolicy>
class simple_ivp::IVPSolver
{
  double h_;
  double t_;
  int nstep_;
  Eigen::VectorXd x_;
  SteppingPolicy stepping_;
  RHSPolicy rhs_;
public:
  IVPSolver(double h, double t, int nstep, Eigen::VectorXd v) :
    h_ {h}, t_ {t}, nstep_{nstep}, x_ {v}, stepping_ {v.size()}, rhs_ {}
  {}
  void solve();
};

template<typename SteppingPolicy, typename RHSPolicy>
void simple_ivp::IVPSolver<SteppingPolicy, RHSPolicy>::solve()
{
  for(int i{0}; i<nstep_; i++) {
    stepping_.step(&rhs_, h_, x_, t_);
    std::cout << x_ << std::endl;
  }
}

#endif

#ifndef RK4_HPP
#define RK4_HPP

#include "stepping_method.hpp"
#include "rhs_ivp.hpp"
#include <iostream>

namespace simple_ivp{
  class Rk4;
}

class simple_ivp::Rk4{
  Eigen::VectorXd k1_;
  Eigen::VectorXd k2_;
  Eigen::VectorXd k3_;
  Eigen::VectorXd k4_;
public:
  explicit Rk4(long int ndim) : k1_(ndim), k2_(ndim), k3_(ndim), k4_(ndim)
  {}
  void step(RHSFunction* rhs, double const& h, Eigen::VectorXd& x, double& t);
};

void simple_ivp::Rk4::step(RHSFunction* rhs, double const& h, Eigen::VectorXd& x, double& t)
{
  double a = h/6.0;
  rhs -> f (x, t, k1_);
  rhs -> f (x + 0.5*h*k1_, t+0.5*h, k2_);
  rhs -> f (x + 0.5*h*k2_, t+0.5*h, k3_);
  rhs -> f (x + h*k3_, t+h, k4_);
  x += a*(k1_ + 2.0*k2_ + 2.0*k3_ + k4_);
  t += h;
}

#endif

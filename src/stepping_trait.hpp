#ifndef FD_TRAIT_HPP
#define FD_TRAIT_HPP

namespace simple_ivp{

  // finite difference trait
  template<typename T> 
  struct is_stepping_method{ 
    static const bool value = false; 
  };

}

#endif

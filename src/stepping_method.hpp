#ifndef STEPPING_METHOD_HPP
#define STEPPING_METHOD_HPP

#include "Eigen/Dense"

namespace simple_ivp{
  class SteppingMethod;
}

class simple_ivp::SteppingMethod{
public:
  virtual ~SteppingMethod(){}
  virtual void step(void (*f)(Eigen::VectorXd const&, double const&, Eigen::VectorXd&),
		    double h, Eigen::VectorXd x, double t) = 0;
};

#endif

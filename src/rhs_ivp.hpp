#ifndef RHS_IVP_HPP
#define RHS_IVP_HPP

#include <Eigen/Dense>

namespace simple_ivp{
  class RHSFunctionBase;
  class RHSFunction;
}

class simple_ivp::RHSFunctionBase{
public:
  virtual ~RHSFunctionBase(){}
  void f(const Eigen::VectorXd& x, double const& t, Eigen::VectorXd& rhs){
    f_(x, t, rhs);
  }
private:
  virtual void f_(const Eigen::VectorXd& x, double const& t, Eigen::VectorXd& rhs) = 0;
};

class simple_ivp::RHSFunction: public simple_ivp::RHSFunctionBase{
  void f_(const Eigen::VectorXd& x, double const& t, Eigen::VectorXd& rhs) {
    for (int i{0}; i<rhs.size(); i++) {rhs(i) = -10.0*x(i);}
  }
};


#endif

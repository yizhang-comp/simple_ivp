#ifndef EULER_HPP
#define EULER_HPP

#include "stepping_method.hpp"
#include "rhs_ivp.hpp"

namespace simple_ivp{
  class ForwardEuler;
}

class simple_ivp::ForwardEuler{
  Eigen::VectorXd work_;
public:
  explicit ForwardEuler(long int ndim) : work_(ndim) {}
  void step(RHSFunction* rhs, double const& h, Eigen::VectorXd& x, double& t);
};

void simple_ivp::ForwardEuler::step(RHSFunction* rhs, double const& h, Eigen::VectorXd& x, double& t)
{
  rhs -> f (x, t, work_);
  x += h*work_;
  t += h;
}
  
#endif
